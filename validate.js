function Validate() {

    var name = $('#name').val();
    var sername = $('#sername').val();
    var fatherName = $('#fatherName').val();
    var email = $('#email').val();
    var age = $('#age').val();
    var flag = true;
    var result = [];

    if (name == '') {
        var subs = 'Введи имя';
        result[0] = subs;
        $('#spanName').html(subs);
        flag = false;
    } else {
        $('#spanName').html('');
    }

    if (sername == '') {
        var subs = 'Введи фамилию';
        result[1] = subs;
        $('#spanSername').html(subs);
        flag = false;
    } else {
        $('#spanSername').html('');
    }

    if (fatherName == '') {
        var subs = 'Отчество можешь не вводить';
        result[2] = subs;
        $('#spanFatherName').html(subs);
    } else {
        $('#spanFatherName').html('');
    }

    var regExp = email.match(/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/i);
    if (email == '') {
        var subs = 'Введи почту';
        result[3] = subs;
        $('#spanEmail').html(subs);
        flag = false;
    } else
    if (regExp == null) {
        var subs = 'Почта введена некорректно';
        result[3] = subs;
        $('#spanEmail').html(subs);
        flag = false;
    } else {
        $('#spanEmail').html('');
    }

    if (age == '') {
        var subs = 'Введи возраст';
        result[4] = subs;
        $('#spanAge').html(subs);
        flag = false;
    } else
    if (parseInt(age) < 18) {
        var subs = 'Тебе должно быть больше 17 лет';
        result[4] = subs;
        $('#spanAge').html(subs);
        flag = false;
    } else {
        $('#spanAge').html('');
    }

    var errors = '';
    result.forEach(function(item, i, result) {
        errors += '<li>' + item + '</li>';
    });

    $('#missUl').html(errors);
    return flag;
}

function Execute() {
    Validate();
    //document.write(name);
}
